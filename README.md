recife.html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <header>
        <span>Recife</span>
        <nav>
            <li>
                <ul>Praça do Marco Zero</ul>
            </li>
        </nav>
    </header>
    <main>
        <h1>Bemvindo a Recife!</h1>
        <h2>Sobre a Cidade</h2>
        <p><B>A Praça Rio Branco, também conhecida como Marco Zero, é um espaço público localizado no bairro do Recife da cidade homônima, capital de Pernambuco.
O local fica próximo ao Porto do Recife e abriga o Marco Zero da cidade do Recife. É deste marco que são feitas todas as medidas oficiais de 
distâncias rodoviárias locais.</B></p>
    </main>
    <section>
        <img src="./recife.jpeg"/>
    </section>
    <footer>
        <ul>
            <li><a href="https://pt.wikipedia.org/wiki/Pra%C3%A7a_Rio_Branco_(Recife)"</a></li>
        </ul>
    </footer>  
</body>
</html>
